<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
		$this->load->model('M_heroUnit');
		$data['hero'] = $this->M_heroUnit->getHero();
		$this->load->view('landing-page', $data);
	}

	public function tambahFoto(){
		$this->load->view('tambah-foto');
	}

	public function uploadFoto(){
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 1000;
		$config['max_width']            = 2048;
		$config['max_height']           = 1080;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file_foto')) {
			$error = array('error' => $this->upload->display_errors());

			echo 'upload gagal';

			// $this->load->view('upload_form', $error);
		} else {
			$data = $this->upload->data();

			$input = $this->input->post();

			$foto['label'] = $input['label'];
			$foto['description'] = $input['description'];
			$foto['file_foto'] = $data['file_name'];
			$foto['status_persetujuan'] = 'belum disetujui';

			$this->db->insert('hero_unit', $foto);
			if ($this->db->affected_rows() > 0) {
				echo 'data berhasil disimpan';
			} else {
				echo 'data gagal disimpan';
			}


			// $this->load->view('upload_success', $data);
		}
	}

	public function hapusFoto(){
		/*
		ambil data yang lama
		di unlink
		upload data baru
		*/
		unlink('./uploads/wallpaperflare_com_wallpaper.jpg');
		echo 'file berhasil dihapus';
	}
}
